<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package AMP for Magento 2
*/

namespace Amasty\Amp\Block\Cms\Home;

use Magento\Framework\View\Element\Template;

class PredefinedContent extends Template
{
    /**
     * @var string
     */
    protected $_template = 'Amasty_Amp::cms/home/ampcontent.phtml';
}
