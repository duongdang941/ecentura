<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package AMP for Magento 2
*/

namespace Amasty\Amp\Helper\Catalog;

use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * create empty to implement interface
 */
class Output extends \Magento\Catalog\Helper\Output implements ArgumentInterface
{

}
