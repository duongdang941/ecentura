<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package AMP for Magento 2
*/

namespace Amasty\Amp\Helper\Catalog;

use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * create empty to implement interface
 */
class Image extends \Magento\Catalog\Helper\Image implements ArgumentInterface
{

}
