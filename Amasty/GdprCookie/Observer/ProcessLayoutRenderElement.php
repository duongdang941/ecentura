<?php
/**
* @author Amasty Team
* @copyright Copyright (c) 2022 Amasty (https://www.amasty.com)
* @package Cookie Consent (GDPR) for Magento 2
*/

namespace Amasty\GdprCookie\Observer;

use Amasty\GdprCookie\Model\Config\Source\CookiePolicyBarStyle;
use Amasty\GdprCookie\Model\ConfigProvider;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Layout;

class ProcessLayoutRenderElement implements ObserverInterface
{
    public const BLOCK_NAME = 'gdprcookie_bar_footer';

    /**
     * @var string[]
     */
    protected $parentBlockPositions = [
        'top' => 'after.body.start',
        'bottom' => 'root'
    ];

    /**
     * @var bool
     */
    private $processed = false;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    public function __construct(
        ConfigProvider $configProvider
    ) {
        $this->configProvider = $configProvider;
    }

    public function execute(Observer $observer)
    {
        if (!$this->processed && $this->configProvider->isCookieBarEnabled()) {
            $event = $observer->getEvent();
            /** @var Layout $layout */
            $layout = $event->getLayout();
            $blockParent = $this->parentBlockPositions['bottom'] ?? '';

            if ((int)$this->configProvider->getBarLocation()
                && $this->configProvider->getCookiePrivacyBarType() === CookiePolicyBarStyle::CONFIRMATION
            ) {
                $blockParent = $this->parentBlockPositions['top'] ?? '';
            }

            if ($blockParent && $layout->hasElement($blockParent)) {
                $layout->addBlock(
                    \Amasty\GdprCookie\Block\CookieBar::class,
                    self::BLOCK_NAME,
                    $blockParent
                );
            }

            $this->processed = true;
        }
    }
}
