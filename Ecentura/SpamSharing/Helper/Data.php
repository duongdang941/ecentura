<?php

namespace Ecentura\SpamSharing\Helper;

use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 *
 * @package Ecentura\SpamSharing\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_FORMS = "spamsharing/spam_email/forms_controller";
    const XML_PATH_EMAIL_LIST = "spamsharing/spam_email/email_list";
    const XML_PATH_LIMIT = "spamsharing/spam_email/max_send";
    const XML_PATH_PREFIX_EMAIL_LIST = "spamsharing/spam_email/prefix_email_list";
    const XML_PATH_SPAM_ENABLE = "spamsharing/spam_email/enabled";
    const XML_PATH_DISABLE_CUSTOMER_REGISTER = "spamsharing/spam_email/disable_customer_register";
    const XML_PATH_DISABLE_NEWLETTER = "spamsharing/spam_email/disable_newletter";

    /**
     * isEnable
     */
    public function isEnable()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_SPAM_ENABLE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Disable customer
     */
    public function disableCustomerRegister()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_DISABLE_CUSTOMER_REGISTER, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Disable newletter
     */
    public function disableNewletter()
    {
        return (bool)$this->scopeConfig->getValue(self::XML_PATH_DISABLE_NEWLETTER, ScopeInterface::SCOPE_STORE);
    }

    /**
     * get forms controller
     * @return mixed
     */
    public function getFromsController()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_FORMS, ScopeInterface::SCOPE_STORE);
    }

    /**
     * get email restrict
     * @return mixed
     */
    public function getEmailList()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_EMAIL_LIST, ScopeInterface::SCOPE_STORE);
    }

    /**
     * get limit
     * @return mixed
     */
    public function getLimit()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_LIMIT, ScopeInterface::SCOPE_STORE);
    }

    /**
     * get prefix email
     * @return mixed
     */
    public function getPrefixEmailList()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_PREFIX_EMAIL_LIST, ScopeInterface::SCOPE_STORE);
    }
}
