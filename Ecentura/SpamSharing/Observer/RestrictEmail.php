<?php

namespace Ecentura\SpamSharing\Observer;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RestrictEmail implements ObserverInterface
{
    /**
     * @var \Ecentura\SpamSharing\Helper\Data
     */
    private $helperData;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $url;

    /**
     * @var \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing
     */
    protected $spamSharingResource;
    /**
     * @var \Ecentura\SpamSharing\Model\SpamSharingFactory
     */
    protected $spamSharingFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $timezone;

    /**
     * @var \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing\CollectionFactory
     */
    protected $spamSharingCollectionFactory;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @param \Ecentura\SpamSharing\Helper\Data $helperData
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\UrlInterface $url
     * @param \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing $spamSharingResource
     * @param \Ecentura\SpamSharing\Model\SpamSharingFactory $spamSharingFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing\CollectionFactory $spamSharingCollectionFactory
     * @param \Magento\Framework\App\Response\RedirectInterface $redirect
     */
    public function __construct(
        \Ecentura\SpamSharing\Helper\Data $helperData,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\UrlInterface $url,
        \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing $spamSharingResource,
        \Ecentura\SpamSharing\Model\SpamSharingFactory $spamSharingFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing\CollectionFactory $spamSharingCollectionFactory,
        \Magento\Framework\App\Response\RedirectInterface $redirect
    ) {
        $this->helperData = $helperData;
        $this->messageManager = $messageManager;
        $this->url = $url;
        $this->spamSharingResource = $spamSharingResource;
        $this->spamSharingFactory = $spamSharingFactory;
        $this->timezone = $timezone;
        $this->spamSharingCollectionFactory = $spamSharingCollectionFactory;
        $this->redirect = $redirect;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        $actionFullName = strtolower($request->getFullActionName());
        $formsController = $this->helperData->getFromsController();
        $limit = $this->helperData->getLimit();
        $emailList = $this->helperData->getEmailList() ?: '';
        $prefixEmailList = $this->helperData->getPrefixEmailList() ?: '';

        if ($this->helperData->isEnable()) {
            if ($this->helperData->disableCustomerRegister()) {
                if ($actionFullName == 'customer_account_createpost') {
                    $this->messageManager->addErrorMessage(__('Create customer has been disabled.'));
                    $observer->getControllerAction()->getActionFlag()->set('', ActionInterface::FLAG_NO_DISPATCH, true);
                    $url = $this->redirect->getRefererUrl();
                    $observer->getControllerAction()->getResponse()->setRedirect($url)->sendResponse();
                    return;
                }
            }

            if ($this->helperData->disableCustomerRegister()) {
                if ($actionFullName == 'newsletter_subscriber_new') {
                    $this->messageManager->addErrorMessage(__('Create newsletter has been disabled.'));
                    $observer->getControllerAction()->getActionFlag()->set('', ActionInterface::FLAG_NO_DISPATCH, true);
                    $url = $this->redirect->getRefererUrl();
                    $observer->getControllerAction()->getResponse()->setRedirect($url)->sendResponse();
                    return;
                }
            }

            if ($formsController) {
                $formsController = preg_split('/\n|\r\n?/', $formsController);
                $email = $request->getParam('email') ?: $request->getParam('mail');
                if (in_array($actionFullName, $formsController)) {
                    $emailList = array_map('trim', explode(',', $emailList));
                    $prefixEmailList = array_map('trim', explode(',', $prefixEmailList));
                    if ($email) {
                        if (in_array($email, $emailList) || $this->getEmailMatch($prefixEmailList, $email)) {
                            $this->messageManager->addErrorMessage(__('Your email address is restricted'));
                            $observer->getControllerAction()->getActionFlag()->set('', ActionInterface::FLAG_NO_DISPATCH, true);
                            $url = $this->redirect->getRefererUrl();
                            $observer->getControllerAction()->getResponse()->setRedirect($url)->sendResponse();
                            return;
                        }
                    }

                    $getSpamCollection = $this->getSpamCollection($actionFullName, $email);
                    if ((int)$getSpamCollection->getFirstItem()->getTotalSend() < (int)$limit) {
                        $this->saveSpamEmail($getSpamCollection, $actionFullName, $email);
                    } else {
                        $this->messageManager->addErrorMessage(__('You have reached maximum time to email per day.'));
                        $observer->getControllerAction()->getActionFlag()->set('', ActionInterface::FLAG_NO_DISPATCH, true);
                        $url = $this->redirect->getRefererUrl();
                        $observer->getControllerAction()->getResponse()->setRedirect($url)->sendResponse();
                        return;
                    }
                }
            }
        }
    }

    /**
     * @param $array
     * @param $str
     * @return array
     */
    private function getEmailMatch($array, $str)
    {
        return array_filter($array, function ($haystack) use ($str) {
            return (strpos($str, $haystack));
        });
    }

    /**
     * @param $collection
     * @param $actionFullName
     * @param $email
     * @return void
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    private function saveSpamEmail($collection, $actionFullName, $email)
    {
        $date = $this->timezone->date()->format('Y-m-d');
        if ($collection->getSize() <= 0) {
            $data = [
                'forms_controller' => $actionFullName,
                'email' => $email,
                'total_send' => 1,
                'update_time' => $date,
            ];
            $shareModel = $this->spamSharingFactory->create()->addData($data);
            $this->spamSharingResource->save($shareModel);
        } else {
            $shareModel = $this->spamSharingFactory->create()->load($collection->getFirstItem()->getId());
            $shareModel->setTotalSend($shareModel->getTotalSend() + 1);
            $this->spamSharingResource->save($shareModel);
        }
    }

    /**
     * @param $actionFullName
     * @return mixed
     */
    private function getSpamCollection($actionFullName, $email)
    {
        $date = $this->timezone->date()->format('Y-m-d');
        $collection = $this->spamSharingCollectionFactory->create();
        $collection->addFieldToFilter('forms_controller', $actionFullName);
        $collection->addFieldToFilter('email', $email);
        $collection->addFieldToFilter('update_time', $date);
        return $collection;
    }
}
