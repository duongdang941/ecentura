<?php

namespace Ecentura\SpamSharing\Model\ResourceModel;

class SpamSharing extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('spamsharing', 'id');
    }
}
