<?php

namespace Ecentura\SpamSharing\Model\ResourceModel\SpamSharing;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            \Ecentura\SpamSharing\Model\SpamSharing::class,
            \Ecentura\SpamSharing\Model\ResourceModel\SpamSharing::class
        );
    }
}
