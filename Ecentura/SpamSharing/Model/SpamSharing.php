<?php

namespace Ecentura\SpamSharing\Model;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class SpamSharing extends AbstractModel
{
    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'Ecentura_spamsharing';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init(\Ecentura\SpamSharing\Model\ResourceModel\SpamSharing::class);
    }
}
