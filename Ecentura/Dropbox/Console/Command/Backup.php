<?php

namespace Ecentura\Dropbox\Console\Command;

use Magento\Framework\Config\File\ConfigFilePool;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Backup extends Command
{
    /**
     * @var
     */
    private $token;

    /**
     * @var int
     */
    private $chunkSize = 150000000;

    /**
     * @var \Magento\Framework\App\State
     */
    protected $state;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var \Magento\Framework\Setup\BackupRollbackFactory
     */
    protected $backupRollbackFactory;

    /**
     * @var \Magento\Framework\App\DeploymentConfig\FileReader
     */
    protected $fileReader;

    /**
     * @var \Ecentura\Dropbox\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $configWriter;

    /**
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Setup\BackupRollbackFactory $backupRollbackFactory
     * @param \Magento\Framework\App\DeploymentConfig\FileReader $fileReader
     * @param \Ecentura\Dropbox\Helper\Data $dataHelper
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param string|null $name
     */
    public function __construct(
        \Magento\Framework\App\State $state,
        \Magento\Framework\Filesystem\DirectoryList $directoryList,
        \Magento\Framework\Setup\BackupRollbackFactory $backupRollbackFactory,
        \Magento\Framework\App\DeploymentConfig\FileReader $fileReader,
        \Ecentura\Dropbox\Helper\Data $dataHelper,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        string $name = null
    ) {
        $this->state = $state;
        $this->directoryList = $directoryList;
        $this->backupRollbackFactory = $backupRollbackFactory;
        $this->fileReader = $fileReader;
        $this->dataHelper = $dataHelper;
        $this->configWriter = $configWriter;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('dropbox:backup');
        $this->setDescription('Dropbox backup');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/dropbox_backup.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info(__('Start backup ---------------------------'));

        try {
            $this->token = $this->getToken();
            $this->backupDB();
            $this->backupCode();
        } catch (\Exception $exception) {
            $logger->err($exception->getMessage() . " line " . $exception->getLine());
        }

        $logger->info(__('End backup ---------------------------'));
        return 0;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getToken()
    {
        $clientId = $this->dataHelper->getClientKey();
        $clientSecret = $this->dataHelper->getClientSecret();
        $freshToken = $this->getRefreshToken();

        $url = 'https://api.dropbox.com/oauth2/token';
        $data = array(
            'refresh_token' => $freshToken,
            'grant_type' => 'refresh_token',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $queryString = http_build_query($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $responseArr = json_decode($response, true);
        curl_close($ch);

        if (isset($responseArr['error'])) {
            throw new \Exception($responseArr['error']);
        }

        return $responseArr['access_token'];
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getRefreshToken()
    {
        $refreshToken = $this->dataHelper->getRefreshToken();

        if ($refreshToken) {
            return $refreshToken;
        }

        $code = $this->dataHelper->getAuthorizationCode();
        $clientId = $this->dataHelper->getClientKey();
        $clientSecret = $this->dataHelper->getClientSecret();
        $url = 'https://api.dropbox.com/oauth2/token';

        $data = array(
            'code' => $code,
            'grant_type' => 'authorization_code',
            'client_id' => $clientId,
            'client_secret' => $clientSecret
        );

        $queryString = http_build_query($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $queryString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $responseArr = json_decode($response, true);
        curl_close($ch);

        if (isset($responseArr['error'])) {
            throw new \Exception($responseArr['error']);
        }

        $this->configWriter->save(\Ecentura\Dropbox\Helper\Data::REFRESH_TOKEN, $responseArr['refresh_token']);

        return $responseArr['refresh_token'];
    }

    /**
     * Upload file to Dropbox
     *
     * @param $filePath
     * @return void
     * @throws \Exception
     */
    private function uploadToDropbox($filePath)
    {
        $appendUrl = 'https://content.dropboxapi.com/2/files/upload_session/append_v2';
        $startUrl = 'https://content.dropboxapi.com/2/files/upload_session/start';
        $finishUrl = 'https://content.dropboxapi.com/2/files/upload_session/finish';

        $destinationFolder = $this->dataHelper->getDropboxDestination() ? $this->dataHelper->getDropboxDestination() . '/' . date("d-m-Y") : date("d-m-Y");

        $headers = array(
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/octet-stream',
            'Dropbox-API-Arg: ' .
            json_encode(
                array(
                    "close" => false
                )
            )

        );

        $chunkSize = $this->chunkSize;
        $fp = fopen($filePath, 'rb');
        $filesize = filesize($filePath);
        $toSend = $filesize;
        $first = $toSend > $chunkSize ? $chunkSize : $toSend;

        $ch = curl_init($startUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, fread($fp, $first));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $responseArray = json_decode($response, true);

        if (isset($responseArray['error'])) {
            throw new \Exception(__($responseArray['error']));
        }
        $session = $responseArray['session_id'];
        $toSend -= $first;
        $position = $first;

        $infoArray["cursor"] = array();
        $infoArray["cursor"]["session_id"] = $session;

        while ($toSend > $chunkSize) {
            $infoArray["cursor"]["offset"] = $position;
            $headers[2] = 'Dropbox-API-Arg: ' . json_encode($infoArray);

            curl_setopt($ch, CURLOPT_URL, $appendUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, fread($fp, $chunkSize));
            curl_exec($ch);

            $toSend -= $chunkSize;
            $position += $chunkSize;
        }

        unset($infoArray["close"]);
        $infoArray["cursor"]["offset"] = $position;
        $infoArray["commit"] = array();
        $infoArray["commit"]["path"] = '/' . $destinationFolder . '/' . basename($filePath);
        $infoArray["commit"]["mode"] = array();
        $infoArray["commit"]["mode"][".tag"] = "overwrite";
        $infoArray["commit"]["autorename"] = true;
        $infoArray["commit"]["mute"] = false;
        $infoArray["commit"]["strict_conflict"] = false;
        $headers[2] = 'Dropbox-API-Arg: ' . json_encode($infoArray);

        curl_setopt($ch, CURLOPT_URL, $finishUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $toSend > 0 ? fread($fp, $toSend) : null);
        curl_exec($ch);
        curl_close($ch);

        fclose($fp);
        unlink($filePath);
    }

    /**
     * Create a db backup and upload to Dropbox
     *
     * @return void
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function backupDB()
    {
        $dbConfig = $this->getDatabaseConfig();
        $dbName = $dbConfig["dbname"];
        $username = $dbConfig["username"];
        $password = $dbConfig["password"];

        $backupPath = $this->directoryList->getPath('var') . '/backups/' . $dbName . '_' . date("d-m-Y") . '.sql.gz';
        exec("mysqldump -u{$username} -p{$password} {$dbName} | gzip > {$backupPath}");
        $this->uploadToDropbox($backupPath);
    }


    /**
     * Create a full backup and upload to Dropbox
     *
     * @return void
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function backupCode()
    {
        $rootPath = $this->directoryList->getRoot();
        $zipFilePath = $this->directoryList->getPath('var') . '/backups/sitebackup_' . date("d-m-Y") . '.zip';
        exec("zip -r {$zipFilePath} {$rootPath}");
        $this->uploadToDropbox($zipFilePath);
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function getDatabaseConfig()
    {
        $envConfig = $this->fileReader->load(ConfigFilePool::APP_ENV);
        return $envConfig["db"]["connection"]["default"];
    }
}