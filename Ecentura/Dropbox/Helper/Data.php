<?php

namespace Ecentura\Dropbox\Helper;

use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const CLIENT_KEY = 'dropbox/general/client_key';
    const CLIENT_SECRET = 'dropbox/general/client_secret';
    const AUTHORIZATION_CODE = 'dropbox/general/authorization_code';
    const DESTINATION = 'dropbox/general/destination';
    const REFRESH_TOKEN = 'dropbox/general/refresh_token';

    /**
     * @return mixed
     */
    public function getClientKey()
    {
        return $this->scopeConfig->getValue(self::CLIENT_KEY, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->scopeConfig->getValue(self::CLIENT_SECRET, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getAuthorizationCode()
    {
        return $this->scopeConfig->getValue(self::AUTHORIZATION_CODE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getDropboxDestination()
    {
        return $this->scopeConfig->getValue(self::DESTINATION, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getRefreshToken()
    {
        return $this->scopeConfig->getValue(self::REFRESH_TOKEN, ScopeInterface::SCOPE_STORE);
    }
}